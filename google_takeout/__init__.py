'''A reader for Google Takeout data exports.

This program can process exported data from Google Takeout and present
it in a human-readable format. Currently, the program supports the export
type(s) listed below:

* Hangouts: Extracts data from Hangouts.json file.

The `google-takeout` program is the user-facing interface and it wraps
other implementation commands. A few usage examples:

    $ google-takeout --help
    ...
    $ google-takeout hangouts --help
    ...
    $ google-takeout hangouts --json-file Hangouts.json
    ...

While the command `google-takeout-hangouts` could be used directly, it is
recommended that `google-takeout hangouts` be used instead.

'''