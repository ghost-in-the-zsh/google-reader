#!/usr/bin/env python3

import sys
import json
import logging
import argparse as ap
import subprocess as sp

from os import execvp
from os.path import basename
from collections import OrderedDict
from typing import Text, List, Tuple

from google_takeout.settings import VERSION


_help_flags     = (
    '-h',
    '--help'
)
_valid_commands = ({
    'hangouts': {
        'description': 'Read and extract data from Google Hangouts file exports',
        'formatter_class': ap.ArgumentDefaultsHelpFormatter,
        'help': 'read .json files from Google Hangouts'
    }
},)


def _get_help_request_meta():
    '''Checks if a help flag was passed in and gets info about it.

    The position of the help flag in the arguments list is important.
    For example, the following commands are not equivalent:

        $ google-takeout -h
        $ google-takeout <command> -h

    The first wants this script to show its help info, whereas the second
    one is intended to show help for the <command> command, which means
    it must be forwarded to the google-takeout-<command> script and this
    one must avoid consuming it.
    '''
    # identify which flag was used to request help/usge info, if any
    help_flag = None
    for flag in _help_flags:
        if flag in sys.argv:
            help_flag = flag
            break

    # We only need to forward help to an external program if the
    # help flag is at an index other than 1; if it's at index position
    # 1, then the help request is for us to process right now and not
    # meant to be forwarded for other commands.
    #
    # We look at `idx - 1` because, if we must forward the help request,
    # then it must be re-inserted into the correct position relative to
    # other pre-existing arguments that will get moved around when the
    # help flag is temporarily removed.
    help_flag_idx = sys.argv.index(help_flag) - 1 if help_flag else None
    forward_help  = True if help_flag_idx and help_flag_idx > 0 else False

    return (forward_help, help_flag, help_flag_idx)


def _get_parsed_arguments() -> Tuple[ap.Namespace, List[str]]:
    '''Return a Namespace with only a partial parsing of the arguments.

    This is necessary because some of the arguments are really meant to
    be forwarded to other scripts rather than being processed and
    consumed by this one.
    '''
    parser = ap.ArgumentParser(
        description='Read and extract data from Google Takeout file exports',
        formatter_class=ap.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version='%(prog)s {}'.format(VERSION)
    )
    cmd_parser = parser.add_subparsers(dest='command')
    cmd_parser.required = True
    for cmd_dict in _valid_commands:
        for cmd_key in cmd_dict:
            cmd = cmd_dict[cmd_key]
            cmd_parser.add_parser(
                cmd_key,
                description=cmd['description'],
                formatter_class=cmd['formatter_class'],
                help=cmd['help']
            )
    # We want to parse the args, but only partially and up to the
    # first positional arg. The rest of the arguments are meant to
    # be forwarded to external scripts and we should avoid consuming
    # them.
    return parser.parse_known_args()


def _gen_command(command: Text, subargs: List[Text], forward: bool, flag: Text, flag_idx: int) -> List[Text]:
    '''Generates the command to be executed by this wrapper program.

    This script is a wrapper that forwards commands and their options to
    implementation scripts. This function builds the command that will
    be executed and handles user requests for help, if any.
    '''
    # The command format is: google-takeout-<command> [options]
    cmd_name = basename(sys.argv[0]).split('.')[0] + f'-{command}'
    full_cmd = [cmd_name] + subargs
    if forward:
        full_cmd.insert(flag_idx, flag)
    return full_cmd


def main():
    # The help flag and index are only considered 'valid' if help
    # needs to be forwarded to a sub-command (i.e. forward_help is True)
    forward_help, help_flag, help_flag_idx = _get_help_request_meta()

    # If the user asked for help on a sub-command, then we have to remove
    # it here before asking the parser to parse the arguments; otherwise
    # the parser will consume the help flag prematurely and the help info
    # for *this* script will be shown, which is not what the user wants.
    #
    # To handle this, we 1) remove the flag; 2) parse the args; then
    # 3) re-insert the flag again, before spawning the sub-process.
    if forward_help:
        sys.argv.remove(help_flag)

    args, subargs = _get_parsed_arguments()
    full_cmd = _gen_command(
        args.command,
        subargs,
        forward_help,
        help_flag,
        help_flag_idx
    )

    # The command name must be included as part of the command args b/c,
    # when Python is invoked, it sets sys.argv to everything *except*
    # its own sys.executable. Not including the command name as part of
    # the args would cause the 1st command arg to be ignored when using
    # os.execvp(...), causing the sub-command for the child process to be
    # lost/dropped.
    #
    # See 2nd paragraph:
    #   https://docs.python.org/3/library/os.html?highlight=os.exec#process-management
    command_name = full_cmd[0]
    command_args = full_cmd
    try:
        sys.stdin.flush()
        sys.stdout.flush()
        sys.stderr.flush()
        execvp(command_name, command_args)
    except (OSError, NotImplementedError) as e:
        msg = f'Error running command \'{command_name} {command_args}\': {str(e)}'
        print(msg, file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
