
import os
import json

from datetime import datetime
from unittest import TestCase

from google_takeout.hangouts.models import \
    User,                       \
    EventFactory,               \
    Event,                      \
    NullEvent,                  \
    TextMessageEvent,           \
    MediaMessageEvent,          \
    HangoutEvent,               \
    MembershipChangeEvent,      \
    RenameConversationEvent,    \
    Conversation


# The fully qualified name of the models module being tested;
# used when testing repr output, which includes full path to
# module and class name.
_test_module_prefix = 'google_takeout.hangouts.models'


# Dummy conversation to avoid hardcoding large dictionaries and duplication.
_filepath = os.path.join('google_takeout', 'tests', 'hangouts', 'sample.json')
with open(_filepath, 'r') as f:
    _sample_conversation = json.loads(f.read())


_event_nodes = {
    'media_message_event'       : _sample_conversation['events'][0],
    'text_message_event'        : _sample_conversation['events'][1],
    'conversation_rename_event' : _sample_conversation['events'][2],
    'membership_change_event'   : _sample_conversation['events'][3],
    'hangout_event'             : _sample_conversation['events'][4]
}


def _get_event_repr(e):
    return '<{}.{}: id="{}" sender="{}" datetime="{}">'.format(
        _test_module_prefix,
        e.__class__.__name__,
        e.id,
        e.sender.name,
        e.datetime
    )

def _get_event_str(e):
    return '[{}] {}: "{}"'.format(
        str(e.datetime),
        e.sender.name,
        e.content
    )


class UserTestCase(TestCase):

    _GAIA_ID        = 'UserID_00'
    _FALLBACK_NAME  = 'UserName_00'

    def setUp(self):
        user_node = _sample_conversation['conversation']['conversation']['participant_data'][0]
        self.user = User(user_node)

    def test_user_id_property_matches(self):
        self.assertEqual(self.user.id, UserTestCase._GAIA_ID)

    def test_user_name_property_matches(self):
        self.assertEqual(self.user.name, UserTestCase._FALLBACK_NAME)

    def test_user_is_stored_in_cache(self):
        self.assertEqual(User.get(UserTestCase._GAIA_ID).id, UserTestCase._GAIA_ID)

    def test_repr_method_matches(self):
        expected  = '<{}.User: id="{}" name="{}">'.format(
            _test_module_prefix,
            self.user.id,
            self.user.name
        )
        self.assertEqual(repr(self.user), expected)

    def test_str_method_returns_name(self):
        self.assertEqual(str(self.user), UserTestCase._FALLBACK_NAME)


class EventFactoryTestCase(TestCase):
    def setUp(self):
        self._media_message_event       = _event_nodes['media_message_event']
        self._text_message_event        = _event_nodes['text_message_event']
        self._conversation_rename_event = _event_nodes['conversation_rename_event']
        self._membership_change_event   = _event_nodes['membership_change_event']
        self._hangout_event             = _event_nodes['hangout_event']

        # only need to cache the user for event.sender, so there's
        # no need to keep it around locally
        user_node0 = _sample_conversation['conversation']['conversation']['participant_data'][0]
        user_node1 = _sample_conversation['conversation']['conversation']['participant_data'][1]
        User(user_node0)
        User(user_node1)

    def test_media_message_event_type(self):
        e = EventFactory.create_event(self._media_message_event)
        self.assertEqual(type(e), MediaMessageEvent)

    def test_text_message_event_type(self):
        e = EventFactory.create_event(self._text_message_event)
        self.assertEqual(type(e), TextMessageEvent)

    def test_rename_conversation_event_type(self):
        e = EventFactory.create_event(self._conversation_rename_event)
        self.assertEqual(type(e), RenameConversationEvent)

    def test_membership_change_event_type(self):
        e = EventFactory.create_event(self._membership_change_event)
        self.assertEqual(type(e), MembershipChangeEvent)

    def test_hangout_event_type(self):
        e = EventFactory.create_event(self._hangout_event)
        self.assertEqual(type(e), HangoutEvent)


class EventTestCase(TestCase):
    _EVENT_ID       = 'EventID_00'
    _SENDER_GAIA_ID = 'UserID_01'
    _UNIX_TIMESTAMP = 1447273146225317    # in microseconds
    _DATETIME       = datetime.utcfromtimestamp(_UNIX_TIMESTAMP / 10**6).astimezone()

    class DummyEvent(Event):
        '''Test event to verify base Event functionality.'''
        def __init__(self, event):
            super().__init__(event)

        @property
        def content(self):
            # invoke the parent's property to verify
            # the expected error is raised
            return super().content  # pylint: disable=no-member

        def __repr__(self):
            return super().__repr__()

    def setUp(self):
        event_node = _sample_conversation['events'][0]
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][1]
        self.user  = User(user_node)
        self.event = EventTestCase.DummyEvent(event_node)

    def test_event_id_property_matches(self):
        self.assertEqual(self.event.id, EventTestCase._EVENT_ID)

    def test_abstract_content_property_raises_error(self):
        # Event is an ABC, and content is an abstract property
        # with its default implementation raising this error
        with self.assertRaises(NotImplementedError):
            self.event.content

    def test_unix_timestamp_property_matches(self):
        self.assertEqual(self.event.timestamp, EventTestCase._UNIX_TIMESTAMP)

    def test_datetime_property_matches(self):
        self.assertEqual(self.event.datetime, EventTestCase._DATETIME)

    def test_sender_property_matches(self):
        self.assertEqual(self.event.sender.id, EventTestCase._SENDER_GAIA_ID)

    def test_repr_method_matches(self):
        self.assertEqual(repr(self.event), _get_event_repr(self.event))

    def test_str_method_returns_formatted_event(self):
        # raised b/c the base Event needs the content
        # field, which is not implemented
        with self.assertRaises(NotImplementedError):
            str(self.event)


class NullEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['media_message_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][1]
        # only need to cache the user for event.sender, so there's
        # no need to keep it around locally
        User(user_node)
        self.event = NullEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, repr(self.event))

    def test_repr_method_matches(self):
        self.assertEqual(repr(self.event), _get_event_repr(self.event))

    def test_str_method_matches(self):
        self.assertEqual(repr(self.event), _get_event_repr(self.event))


class MediaMessageEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['media_message_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][0]
        self.user  = User(user_node)
        self.event = MediaMessageEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, 'https://example.com/original')

    def test_repr_method_matches(self):
        self.assertEqual(repr(self.event), _get_event_repr(self.event))

    def test_str_method_matches(self):
        self.assertEqual(str(self.event), _get_event_str(self.event))


class TextMessageEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['text_message_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][1]
        self.user  = User(user_node)
        self.event = TextMessageEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, 'This is a sample text message')

    def test_repr_method_matches(self):
        self.assertEqual(repr(self.event), _get_event_repr(self.event))

    def test_str_method_matches(self):
        self.assertEqual(str(self.event), _get_event_str(self.event))


class HangoutEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['hangout_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][1]
        self.user  = User(user_node)
        self.event = HangoutEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, repr(self.event))

    def test_repr_method_matches(self):
        exp = '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" media_type="{}">'.format(
                _test_module_prefix,
                self.event.__class__.__name__,
                self.event.id,
                self.event.sender.name,
                self.event.datetime,
                'start_hangout',
                'audio_video'
            )
        self.assertEqual(repr(self.event), exp)

    def test_str_method_matches(self):
        self.assertEqual(str(self.event), _get_event_str(self.event))


class MembershipChangeEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['membership_change_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][0]
        self.user  = User(user_node)
        self.event = MembershipChangeEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, repr(self.event))

    def test_repr_method_matches(self):
        exp = '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" participant="{}">'.format(
                _test_module_prefix,
                self.event.__class__.__name__,
                self.event.id,
                self.event.sender.name,
                self.event.datetime,
                'add_user',
                'UserName_01'
            )
        self.assertEqual(repr(self.event), exp)

    def test_str_method_matches(self):
        self.assertEqual(str(self.event), _get_event_str(self.event))


class RenameConversationEventTestCase(TestCase):
    def setUp(self):
        event_node = _event_nodes['conversation_rename_event']
        user_node  = _sample_conversation['conversation']['conversation']['participant_data'][0]
        self.user  = User(user_node)
        self.event = RenameConversationEvent(event_node)

    def test_content_property_matches(self):
        self.assertEqual(self.event.content, repr(self.event))

    def test_repr_method_matches(self):
        exp = '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" old_name="{}" new_name="{}">'.format(
                _test_module_prefix,
                self.event.__class__.__name__,
                self.event.id,
                self.event.sender.name,
                self.event.datetime,
                'rename_conversation',
                'Old Conversation Name',
                'New Conversation Name'
            )
        self.assertEqual(repr(self.event), exp)

    def test_str_method_matches(self):
        self.assertEqual(str(self.event), _get_event_str(self.event))


class ConversationTestCase(TestCase):
    def setUp(self):
        user_node0 = _sample_conversation['conversation']['conversation']['participant_data'][0]
        user_node1 = _sample_conversation['conversation']['conversation']['participant_data'][1]
        # only need to cache the user for event.sender, so there's
        # no need to keep it around locally
        User(user_node0)
        User(user_node1)
        self.conv = Conversation(_sample_conversation)

    def test_conversation_id_matches(self):
        self.assertEqual(self.conv.id, 'Conversation-ID')

    def test_conversation_name_matches(self):
        # conversation has no name/title, so it defaults to <N/A>
        self.assertEqual(self.conv.name, '<N/A>')

    def test_conversation_otr_status_matches(self):
        self.assertEqual(self.conv.otr_status, 'ON_THE_RECORD')

    def test_conversation_group_status_matches(self):
        # This one is STICKY_ONE_TO_ONE, not GROUP, so False is expected
        self.assertEqual(self.conv.is_group_conversation, False)

    def test_conversation_on_the_record_matches(self):
        # This one is ON_THE_RECORD, so True is expected.
        # See test case on OTR status.
        self.assertEqual(self.conv.is_on_the_record, True)

    def test_conversation_user_count_matches(self):
        self.assertEqual(len(self.conv.users), 2)

    def test_conversation_events_count_matches(self):
        self.assertEqual(len(self.conv.events), 15)

    def test_conversation_repr_method_matches(self):
        exp = '<{}.{}: id="{}" name="{}" group_chat="{}" on_record="{}" participants={:,} events={:,}>'.format(
            _test_module_prefix,
            self.conv.__class__.__name__,
            self.conv.id,
            self.conv.name,
            'No',
            'Yes',
            len(self.conv.users),
            len(self.conv.events)
        )
        self.assertEqual(repr(self.conv), exp)

    def test_conversation_str_method_matches(self):
        self.assertEqual(
            str(self.conv),
            '{}: {}'.format(self.conv.name, self.conv.id)
        )
