
import os
import logging

from datetime import datetime as DateTime
from typing import Dict, List, Text
from abc import ABCMeta, abstractproperty


_logger = logging.getLogger(__name__)


class User:

    # Must keep a cache of all the users, regardless of the conversations
    # in which they show up; needed b/c some conversations will have
    # events from users that were not (originally?) part of the conversation
    # and failed to show up under its 'participant_data' node when
    # originally inspected; these end up causing KeyErrors when they
    # need to be looked up.
    #
    # Cannot use @functools.lru_cache because this class' argument is a
    # dictionary and not hashable, which is a requirement for the caching
    # decorator.
    _cache = dict()

    def __init__(self, node: Dict):
        self._id   = node['id']['gaia_id']
        self._name = node['fallback_name']

        # auto-update the users cache
        User._cache[self._id] = self

    @property
    def id(self) -> Text:
        return self._id

    @property
    def name(self) -> Text:
        return self._name

    @classmethod
    def get(cls, id: str):
        return User._cache[id]

    def __repr__(self):
        return '<{}.{}: id="{}" name="{}">'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.name
        )

    def __str__(self):
        return self.name


class Event(metaclass=ABCMeta):
    def __init__(self, event: Dict):
        self._id        = event['event_id']
        self._timestamp = int(event['timestamp'])   # unix/epoch time, in microseconds
        self._datetime  = DateTime.utcfromtimestamp(self._timestamp / 10**6).astimezone()
        self._sender    = User.get(event['sender_id']['gaia_id'])

    @property
    def id(self) -> Text:
        return self._id

    @abstractproperty
    def content(self) -> Text:
        raise NotImplementedError()

    @property
    def datetime(self) -> DateTime:
        return self._datetime

    @property
    def timestamp(self) -> int:
        return self._timestamp

    @property
    def sender(self) -> User:
        return self._sender

    def __repr__(self):
        return '<{}.{}: id="{}" sender="{}" datetime="{}">'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.sender.name,
            self.datetime
        )

    def __str__(self):
        return '[{}] {}: "{}"'.format(
            str(self._datetime),
            self.sender.name,
            self.content
        )


class NullEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)

    @property
    def content(self) -> Text:
        return repr(self)

    def __str__(self):
        return repr(self)


class TextMessageEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)
        content = event['chat_message']['message_content']
        self._segments = [TextSegment(s) for s in content['segment']]
        self._content  = ''.join(s.text for s in self._segments)

    @property
    def content(self) -> Text:
        return self._content


class TextSegment:
    def __init__(self, seg: Dict):
        # some segments are of type 'LINE_BREAK'; older versions
        # included a newline char, but more recent ones ommit it,
        # which can raise KeyError; simply add the newline explicitly
        # in those cases
        self._text = seg['text'] if 'text' in seg else os.linesep
        self._type = seg['type']
        if self._type == 'LINE_BREAK':
            _logger.debug('Processed LINE_BREAK')

    @property
    def text(self) -> Text:
        return self._text

    def __str__(self) -> Text:
        return '{}: {}'.format(self._type, self._text)


class MediaMessageEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)
        content = event['chat_message']['message_content']
        self._attachments = [MediaAttachment(a) for a in content['attachment']]
        self._content     = ''.join(a.url for a in self._attachments)

    @property
    def content(self) -> Text:
        return self._content


class MediaAttachment:
    def __init__(self, att: Dict):
        item  = att['embed_item']
        itype = item['type'][0].lower()
        media = item[itype]
        self._id  = att['id']
        if itype == 'plus_audio_v2':
            # plus_photo, plus_audio_v2
            self._media_type    = itype
            self._url           = media['url']
            self._original_url  = self._url
        else:
            # photo, animated_photo, video
            self._media_type    = media['media_type'].lower()
            self._url           = media['url']
            self._original_url  = media['original_content_url']

    @property
    def url(self) -> Text:
        return self._original_url

    def __str__(self) -> Text:
        return '{}: {}'.format(
            self._media_type,
            self._original_url
        )


class HangoutEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)
        evt = event['hangout_event']
        self._evt_type   = evt['event_type'].lower()
        self._media_type = evt['media_type'].lower()

    @property
    def content(self) -> Text:
        return repr(self)

    def __repr__(self):
        return '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" media_type="{}">'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.sender.name,
            self.datetime,
            self._evt_type,
            self._media_type
        )


class MembershipChangeEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)
        chg = event['membership_change']
        self._evt_type = event['event_type'].lower()
        self._user     = User.get(chg['participant_id'][0]['gaia_id'])

    @property
    def content(self) -> Text:
        return repr(self)

    def __repr__(self):
        return '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" participant="{}">'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.sender.name,
            self.datetime,
            self._evt_type,
            self._user.name
        )


class RenameConversationEvent(Event):
    def __init__(self, event: Dict):
        super().__init__(event)
        evt = event['conversation_rename']
        self._evt_type = event['event_type'].lower()
        self._old_name = evt['old_name']
        self._new_name = evt['new_name']
        self._user     = User.get(event['self_event_state']['user_id']['gaia_id'])

    @property
    def content(self) -> Text:
        return repr(self)

    def __repr__(self):
        return '<{}.{}: id="{}" sender="{}" datetime="{}" event_type="{}" old_name="{}" new_name="{}">'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.sender.name,
            self.datetime,
            self._evt_type,
            self._old_name,
            self._new_name
        )


class EventFactory:
    @classmethod
    def create_event(cls, event: Dict) -> Event:
        try:
            msg = event['chat_message']['message_content']
        except KeyError:
            msg = event

        if 'segment' in msg:
            return TextMessageEvent(event)
        elif 'attachment' in msg:
            return MediaMessageEvent(event)
        elif 'hangout_event' in event:
            return HangoutEvent(event)
        elif 'membership_change' in event:
            return MembershipChangeEvent(event)
        elif 'conversation_rename' in event:
            return RenameConversationEvent(event)
        else:
            _logger.warning('Unknown event type: {}'.format(msg))
            _logger.warning('Returning null event')
            return NullEvent(event)


class Conversation:
    def __init__(self, node: Dict):
        conv             = node['conversation']['conversation']
        self._id         = conv['id']['id']
        self._type       = conv['type']     # GROUP, STICKY_ONE_TO_ONE
        self._is_group   = True if self._type == 'GROUP' else False
        self._name       = conv['name'] if self._is_group else '<N/A>'   # only group has 'name' key
        self._otr_status = conv['otr_status']
        self._users      = {u.id: u for u in [User(n) for n in conv['participant_data']]}
        self._events     = sorted(
            [EventFactory.create_event(e) for e in node['events']],
            key=lambda message: message.timestamp
        )

    @property
    def id(self) -> Text:
        return self._id

    @property
    def name(self) -> Text:
        return self._name

    @property
    def type(self) -> Text:
        return self._type

    @property
    def otr_status(self) -> Text:
        return self._otr_status

    @property
    def is_group_conversation(self) -> bool:
        return self._is_group

    @property
    def is_on_the_record(self) -> bool:
        return True if self._otr_status == 'ON_THE_RECORD' else False

    @property
    def users(self) -> List[User]:
        return list(self._users.values())

    @property
    def events(self) -> List[Event]:
        return self._events

    def __repr__(self):
        return '<{}.{}: id="{}" name="{}" group_chat="{}" on_record="{}" participants={:,} events={:,}>'.format(
            __name__,
            self.__class__.__name__,
            self.id,
            self.name,
            'Yes' if self.is_group_conversation else 'No',
            'Yes' if self.is_on_the_record else 'No',
            len(self.users),
            len(self.events)
        )

    def __str__(self):
        return '{}: {}'.format(self.name, self.id)
