
import os
from abc import ABCMeta, abstractmethod
from typing import List, Text

from google_takeout.hangouts.models import Conversation, Event


class View(metaclass=ABCMeta):
    @abstractmethod
    def render(self, conversation: Conversation) -> Text:
        raise NotImplementedError()


class CompactView(View):
    def render(self, conv: Conversation) -> Text:
        eol = os.linesep
        fmt =  '{}: {}' + eol
        fmt += '{}'
        return fmt.format(
            conv.__class__.__name__,
            sorted([u.name for u in conv.users]),
            eol.join('  ' + str(e) for e in conv.events if e is not None)
        )


class DetailedView(View):
    def render(self, conv: Conversation) -> Text:
        eol = os.linesep
        fmt =  '{}: {}' + eol
        fmt += '    id               : {}'   + eol
        fmt += '    type             : {}'   + eol
        fmt += '    otr_status       : {}'   + eol
        fmt += '    participant_count: {:,}' + eol
        fmt += '    participants     : {}'   + eol
        fmt += '    event_count      : {:,}' + eol
        fmt += '    events           :' + eol
        fmt += '{}'
        userlist = conv.users
        return fmt.format(
            conv.__class__.__name__,
            conv.name,
            conv.id,
            conv.type,
            conv.otr_status,
            len(userlist),
            sorted([u.name for u in userlist]),
            len(conv.events),
            ''.join(self._render_events(conv.events))
        )

    def _render_events(self, events: List[Event]) -> List[Text]:
        eol = os.linesep
        fmt =  '        {}:' + eol
        fmt += '            id       : {}' + eol
        fmt += '            date/time: {}' + eol
        fmt += '            timestamp: {}' + eol
        fmt += '            sender   : {}' + eol
        fmt += '            content  : "{}"' + eol
        return [
            fmt.format(
                e.__class__.__name__,
                e.id,
                e.datetime,
                e.timestamp,
                e.sender.name,
                e.content
            ) for e in events if e is not None
        ]


class SummaryListView(View):
    def render(self, conv: Conversation) -> Text:
        eol =  os.linesep
        fmt =  '{}: {}' + eol
        fmt += '    id               : {}'   + eol
        fmt += '    participant_count: {:,}' + eol
        fmt += '    participants     : {}'   + eol
        fmt += '    event_count      : {:,}' + eol
        userlist = conv.users
        return fmt.format(
            conv.__class__.__name__,
            conv.name,
            conv.id,
            len(userlist),
            sorted([u.name for u in userlist]),
            len(conv.events)
        )
