#!/usr/bin/env python3

import sys
import json
import argparse as ap
import logging

from collections import OrderedDict
from json.decoder import JSONDecodeError

from google_takeout import settings

from google_takeout.hangouts.models import Conversation
from google_takeout.hangouts.views import CompactView, DetailedView, SummaryListView


# order matters for default view selection
_views = OrderedDict((
    ('compact' , CompactView),      # default
    ('detailed', DetailedView),
    ('summary' , SummaryListView),
))


def _parse_arguments() -> ap.Namespace:
    parser = ap.ArgumentParser(
        description='Display Google Hangouts data from exported .json files in a human-readable format',
        formatter_class=ap.ArgumentDefaultsHelpFormatter
    )
    _add_hangouts(parser)
    return parser.parse_args()


def _add_hangouts(parser: ap.ArgumentParser) -> None:
    parser.add_argument(
        '--json-file',
        dest='json_file',
        metavar='PATH',
        type=str,
        default='Hangouts.json',
        help='path to .json file exported from Google Takeout'
    )
    parser.add_argument(
        '--view',
        dest='view',
        metavar='VIEW',
        choices=_views.keys(),
        type=str,
        default=list(_views.keys())[0],
        help='layout used to display the data'
    )
    parser.add_argument(
        '--filter-chat',
        dest='conversation',
        metavar='NAME',
        type=str,
        default='all',
        help='filter chat rooms by name (case-insensitive); unnamed chat rooms are set to "<N/A>"'
    )


def main():
    args = _parse_arguments()

    try:
        with open(args.json_file) as f:
            content = json.loads(f.read())
    except FileNotFoundError:
        print(f'File not found: {args.json_file}')
        sys.exit(1)
    except JSONDecodeError:
        print(f'File does not contain JSON-formatted data (or is corrupted): {args.json_file}')
        sys.exit(2)

    try:
        conversations = (Conversation(node) for node in content['conversations'])
    except KeyError:
        print(f'File is not a Google Hangouts export (or is corrupted): {args.json_file}')
        sys.exit(3)

    conversations = filter(
        lambda c:
            c.name.lower() == args.conversation.lower(),
            conversations
    ) if args.conversation != 'all' else conversations

    view = _views[args.view.lower()]()
    for c in conversations:
        print(view.render(c))

    sys.exit(0)


if __name__ == '__main__':
    main()
