# Google Takeout

The google takeout is a command-line utility to **_locally_** process some of the files exportable via [Google Takeout](https://takeout.google.com/). This program does **_not_** attempt to send your private data anywhere. In fact, it does not create network connections at all. **_All processing is done locally in your own computer._** You must export and download your own personal data from Google's site using your own credentials before you can use this tool.


## Installation

Open a terminal and run
```
$ pip3 install https://gitlab.com/ghost-in-the-zsh/google-takeout/-/archive/master/google-takeout-master.zip
```

After installation, you can get general help using `google-takeout --help` and more command-specific help with `google-takeout <command> --help`.


## Current Feature(s)

1. **Google Hangouts:** Only a subset of the JSON file is supported, but it's enough to extract conversations, the messages in it, and the participants sending and/or reading the messages. After downloading your data, simply extract it from the Zip archive and find a file named `Hangouts.json` inside.


## Examples

### Hangouts

Assuming the default `Hangouts.json` file is in your current directory, you can read all conversations/chats as follows:
```
$ google-takeout hangouts
```

This command relies on default options; it attempts to find the default `Hangouts.json` file in the current directory and display its contents using a `compact` view. The following command is more explicit and still equivalent to the previous one:
```
$ google-takeout hangouts --json-file Hangouts.json --view compact
```

You can get more summarized and detailed views by using the `--view summary` and `--view detailed` options instead. You can also look at a single conversation/chat room, as long as it has a proper name. Assuming you have a chat room named "Friends", you can use the following command to narrow down your results:
```
$ google-takeout hangouts --filter-chat Friends
```

For this to work, the chat room must've been given a name/title by its participants. Otherwise, they remain unnamed and cannot be uniquely filtered, with `<N/A>` being displayed as the name in such cases when using the `detailed` or `summary` views.

You can further filter your results by either forwarding the output as the input to another program (i.e. piping) or redirecting it to another file. For example, if you want to see all messages that include "Alice", you can do the following:
```
$ google-takeout hangouts --filter-chat Friends | grep "Alice"
```
Since the `compact` view is the default, and each message is shown in its own line, you'll see all the messages sent by Alice in addition to any others that mention her by name.


## Windows

The syntax for Windows may vary and often depends on whether you have the Python interpreter in your `PATH` or not. Generally, the commands shown above are expected to work if you change `pip3 install` for `py -m pip install`.

The following should work:

```bash
> py -m pip install https://gitlab.com/ghost-in-the-zsh/google-takeout/-/archive/master/google-takeout-master.zip
```

You must make sure the Python installation directory for scripts is also in your `PATH` (e.g. `C:\Python3\Scripts`). You should be able to call the program directly as follows:

```
> google-takeout.exe [options]
```


## Requirements

Only a Python 3.6.5 interpreter (or later) and access to a GNU+Linux shell (or Windows command prompt) are required, though the command syntax in Windows may look different. The output is written to the system console and should be redirected to other system-specific utilities for further processing.


## Contributions

See this short [contribution guide](CONTRIBUTING.md) for details.


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE).
